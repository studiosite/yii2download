<?php

namespace studiosite\yii2download\filters;

use yii\helpers\ArrayHelper;
use Imagine\Image\Box;

/**
 * Фильтр ресайза изображения по размеру
 *
 * @copyright Студия.сайт
 * @author fromtuba <fromtuba@mail.ru>
 * @author Evgeny Kukharik <jonegkk9@gmail.com>
 */
class ConvertFilter implements \studiosite\yii2download\interfaces\ImageFilterInterface
{
    /**
    * Параметры фильтра по умолчанию
    *
    * @return array
    */
    private static function getDefaultOptions() {
        return [
            'extention' => null, // расширение (jpg|bmp|png|jpeg), по умолчанию остается текущий формат изображения
        ];
    }

    /**
    * Наложение фильтра
    *
    * @param \studiosite\yii2download\models\ImageObject &$image
    * @param array $options Параметры фильтра
    */
    public function filter(\studiosite\yii2download\models\ImageObject &$image, array $options)
    {
    }

    /**
    * Перед применением фильтров к моделе
    *
    * @param \studiosite\yii2download\models\ImageObject $image
    * @param array $options Параметры фильтра
    */
    public function beforeAppend(\studiosite\yii2download\models\ImageObject &$image, array $options)
    {
        $options = ArrayHelper::merge(self::getDefaultOptions(), $options);

        if (!empty($options['extention']))
            $image->setExtention($options['extention']);
    }
}
