<?php

namespace studiosite\yii2download\filters;

use yii\helpers\ArrayHelper;
use wapmorgan\UnifiedArchive\UnifiedArchive;

/**
 * Фильтр сжатия файла
 *
 * @copyright Студия.сайт
 * @author fromtuba <fromtuba@mail.ru>
 * @author Evgeny Kukharik <jonegkk9@gmail.com>
 */
class CompressFilter implements \studiosite\yii2download\interfaces\FileFilterInterface
{
    /**
    * Параметры фильтра по умолчанию
    *
    * @return array
    */
    private static function getDefaultOptions() {
        return [
            'format' => 'zip', // формат поддерживаемый \wapmorgan\UnifiedArchive\UnifiedArchive
        ];
    }

    /**
    * Наложение фильтра
    *
    * @param \studiosite\yii2download\models\FileObject &$file
    * @param array $options Параметры фильтра
    */
    public function filter(\studiosite\yii2download\models\FileObject &$file, array $options)
    {
        $options = ArrayHelper::merge(self::getDefaultOptions(), $options);

        $file->setCallback(function($content, $fileName, $options) {
            UnifiedArchive::archiveNodes($content->sourceFileName, $fileName);
            return true;
        });
    }

    /**
    * Перед применением фильтров к моделе
    *
    * @param \studiosite\yii2download\models\FileObject $image
    * @param array $options Параметры фильтра
    */
    public function beforeAppend(\studiosite\yii2download\models\FileObject &$file, array $options)
    {
        $options = ArrayHelper::merge(self::getDefaultOptions(), $options);

        if (!empty($options['format']))
            $file->setExtention($options['format']);
    }
}
