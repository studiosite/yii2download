<?php

namespace studiosite\yii2download\filters;

use yii\helpers\ArrayHelper;
use Imagine\Image\Box;

/**
 * Фильтр ресайза изображения по размеру
 *
 * @copyright Студия.сайт
 * @author fromtuba <fromtuba@mail.ru>
 * @author Evgeny Kukharik <jonegkk9@gmail.com>
 */
class ThumbnailFilter implements \studiosite\yii2download\interfaces\ImageFilterInterface
{
    /**
    * @const integer Дефолтное качество изображения
    */
    const DEFAULT_QUALITY = 100;

    /**
    * Параметры фильтра по умолчанию
    *
    * @return array
    */
    public static function getDefaultOptions() {
        return [
            'size' => null, // размер стороны квадрата size=width=height. Опция приоритетнее width и height
            'width' => null, // ширина изображения. По умолчанию остается прежней
            'height' => null, // высота изображения. По умолчанию остается прежней
            'quality' => self::DEFAULT_QUALITY, // качество изображение (100 - изображение не сжимается)
        ];
    }

    /**
    * Наложение фильтра
    *
    * @param \studiosite\yii2download\models\ImageObject &$image
    * @param array $options Параметры фильтра
    */
    public function filter(\studiosite\yii2download\models\ImageObject &$image, array $options)
    {
        $options = ArrayHelper::merge(self::getDefaultOptions(), $options);

        // Если определен параметр size, то указываем высоту и ширину изображения равную size
        if ($options['size']!==null) {
            $options['width'] = $options['size'];
            $options['height'] = $options['size'];
        }

        $box = new Box($options['width'], $options['height']);
        $image->content = $image->content->thumbnail($box, \Imagine\Image\ManipulatorInterface::THUMBNAIL_OUTBOUND);

        if ($options['quality']!=self::DEFAULT_QUALITY) {
            $image->setQuality($options['quality']);
        }
    }

    /**
    * Перед применением фильтров к моделе
    *
    * @param \studiosite\yii2download\models\ImageObject $image
    * @param array $options Параметры фильтра
    */
    public function beforeAppend(\studiosite\yii2download\models\ImageObject &$image, array $options)
    {

    }
}
