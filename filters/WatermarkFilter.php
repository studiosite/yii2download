<?php

namespace studiosite\yii2download\filters;

use Yii;
use yii\helpers\ArrayHelper;
use yii\imagine\Image;
use yii\base\Exception;

use Imagine\Image\ImageInterface;
use Imagine\Image\Point;
use Imagine\Image\Box;
use Imagine\Image\ManipulatorInterface;

use studiosite\yii2download\models\ImageObject;
/**
 * Фильтр наложения водных знаков из изображния
 *
 * @copyright Студия.сайт
 * @author fromtuba <fromtuba@mail.ru>
 * @author Evgeny Kukharik <jonegkk9@gmail.com>
 */
class WatermarkFilter implements \studiosite\yii2download\interfaces\ImageFilterInterface
{
    /**
    * @var array Параметры фильтра по умолчанию
    */
    public static function getDefaultOptions() {
        return [
            'image' => null, // путь к изображению водного знака
            'right' => null, // положение от правого края
            'left' => null, // положение от левого края
            'top' => null, // положение от верха
            'bottom' => null, // положение от низа
            'resize' => 1, // изменить размер знака (1 не менять, 1.1 увелечить на 10%)
        ];
    }

    /**
    * Наложение фильтра
    *
    * @param \studiosite\yii2download\models\ImageObject &$image
    * @param array $options Параметры фильтра
    */
    public function filter(ImageObject &$image, array $options)
    {
        $options = ArrayHelper::merge(self::getDefaultOptions(), $options);

        $watermark = $this->getImage($options);
        $options['resize'] = floatval($options['resize']);

        if ($options['resize'] != 1) {
            $watermark = $watermark->resize(new Box(
                $watermark->getSize()->getWidth() * $options['resize'],
                $watermark->getSize()->getHeight() * $options['resize']
            ));
        }

        $indents = $this->getIndents($image, $watermark, $options);

        // по рассчитанным отступам происходит изменение размеров watermark
        $watermark = $watermark->resize(new Box(
            $indents['right'] - $indents['left'],
            $indents['bottom'] - $indents['top']
        ));

        $pointInsert = new Point($indents['left'], $indents['top']);
        $size = $image->content->getSize();
        $watermarkSize = $watermark->getSize();

        if ($indents['left'] < $size->getWidth() && $indents['top'] < $size->getHeight()) {

            $maxWidth = ($indents['left']+$watermarkSize->getWidth() <= $size->getWidth()) ? $watermarkSize->getWidth() : $size->getWidth() - $indents['left'];
            $maxHeight = ($indents['top']+$watermarkSize->getHeight() <= $size->getHeight()) ? $watermarkSize->getHeight() : $size->getHeight() - $indents['top'];

            if ($maxWidth<$watermarkSize->getWidth() || $maxHeight<$watermarkSize->getHeight())
                $watermark->crop(new Point(0, 0), new Box($maxWidth, $maxHeight));

            $image->content->paste($watermark, $pointInsert);
        }
    }

    /**
    * Перед применением фильтров к модели
    *
    * @param \studiosite\yii2download\models\ImageObject $image
    * @param array $options Параметры фильтра
    */
    public function beforeAppend(\studiosite\yii2download\models\ImageObject &$image, array $options)
    {

    }

    protected function getImage(array &$options)
    {
        if (!$options['image'] || !is_file(Yii::getAlias($options['image'])) || !file_exists(Yii::getAlias($options['image']))) {
            throw new Exception(Yii::t(
                'studiosite/yii2-download',
                'Watermark image not found'
            ), 400);
        }

        return Image::getImagine()->open(Yii::getAlias($options['image']));
    }

    /**
     * Преобразование отступа для watermark
     * Если отступ указан в виде строки, метод ожидает формат записи Xpx или X%
     * и произведет расчет нового значения для отступа
     * Параметр $size определяет значение, из которого будет рассчитан
     * процент для отступа
     *
     * @param integer|string $indent
     * @param integer $size
     *
     * @return mixed
     */
    protected function parseIndent($indent, $size)
    {
        if (!is_string($indent)) {
            return $indent;
        }

        $matches = array();
        if (!preg_match("/(\d+)((\%|px)*)/", $indent, $matches)) {
            return null;
        }
        if (isset($matches[2]) && $matches[2] == '%') {
            $value = intval($matches[1]);
            return $size * $value / 100;
        } else {
            return intval($matches[1]);
        }
    }

    protected function getIndents(ImageObject $image, ImageInterface $watermark, array &$options)
    {
        $size = $image->content->getSize();

        $options['left'] = $this->parseIndent($options['left'], $size->getWidth());
        $options['right'] = $this->parseIndent($options['right'], $size->getWidth());
        $options['top'] = $this->parseIndent($options['top'], $size->getHeight());
        $options['bottom'] = $this->parseIndent($options['bottom'], $size->getHeight());

        $indents = array(
            'top' => 0,
            'right' => 0,
            'bottom' => 0,
            'left' => 0
        );

        if ($options['left'] !== null) {
            $indents['left'] = intval($options['left']);
        } else {
            if ($options['right'] !== null) {
                $indents['left'] = $size->getWidth() - ($watermark->getSize()->getWidth() + intval($options['right']));
            } else {
                $indents['left'] = $size->getWidth() / 2 - $watermark->getSize()->getWidth() / 2;
            }
        }

        if ($options['right'] !== null) {
            $indents['right'] = $size->getWidth() - intval($options['right']);
        } else {
            if ($options['left'] !== null) {
                $indents['right'] = intval($options['left']) + $watermark->getSize()->getWidth();
            } else {
                $indents['right'] = $size->getWidth() / 2 + $watermark->getSize()->getWidth() / 2;
            }
        }

        if ($options['top'] !== null) {
            $indents['top'] = intval($options['top']);
        } else {
            if ($options['bottom'] !== null) {
                $indents['top'] = $size->getHeight() - ($watermark->getSize()->getHeight() + intval($options['bottom']));
            } else {
                $indents['top'] = $size->getHeight() / 2 - $watermark->getSize()->getHeight() / 2;
            }
        }

        if ($options['bottom'] !== null) {
            $indents['bottom'] = $size->getHeight() - intval($options['bottom']);
        } else {
            if ($options['top'] !== null) {
                $indents['bottom'] = intval($options['top']) + $watermark->getSize()->getHeight();
            } else {
                $indents['bottom'] = $size->getHeight() / 2 + $watermark->getSize()->getHeight() / 2;
            }
        }

        $hBoth = $options['left'] && $options['right'];
        $vBoth = $options['top'] && $options['bottom'];

        if ($hBoth && !$vBoth) {
            // watermark привязана по горизонтали и не привязана по вертикали
            // необходимо скорректировать изменение высоты watermark
            $newWidth = $indents['right'] - $indents['left'];
            $k = $newWidth / $watermark->getSize()->getWidth();

            $r = $watermark->getSize()->getHeight() * $k;
            $r -= $watermark->getSize()->getHeight();
            $r /= 2;

            $indents['top'] -= intval($r);
            $indents['bottom'] += intval($r);
        } else if (!$hBoth && $vBoth) {
            // watermark привязана по вертикали и не привязана по горизонтали
            // необходимо скорректировать изменение ширины watermark
            $newHeight = $indents['bottom'] - $indents['top'];
            $k = $newHeight / $watermark->getSize()->getHeight();

            $r = $watermark->getSize()->getWidth() * $k;
            $r -= $watermark->getSize()->getWidth();
            $r /= 2;

            $indents['left'] -= intval($r);
            $indents['right'] += intval($r);
        }

        if ($indents['left'] <= 0) {
            $indents['left'] = 0;
        }
        if ($indents['top'] <= 0) {
            $indents['top'] = 0;
        }

        if ($indents['right'] - $indents['left'] <= 0 || $indents['bottom'] - $indents['top'] <= 0) {
            throw new Exception(Yii::t(
                'studiosite/yii2-download',
                'Invalid watermark size'
            ), 400);
        }

        return $indents;
    }
}
