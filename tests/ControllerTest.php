<?php

namespace studiosite\yii2download\tests;

use Yii;
use yii\base\InlineAction;
use studiosite\yii2download\controllers\DownloadController;

class ControllerTest extends TestCase
{
    public function testControllerImage()
    {
        $this->mockWebApplication([
            'components' => [
                'request' => [
                    'queryParams' => [
                        'component' => 'imageWithAccess',
                        'name' => 'fixture/bender.png',
                    ]
                ]
            ]
        ]);

        $controller = new DownloadController('download', Yii::$app);
        $response = $controller->runAction('image');

        $this->assertEquals($response->getStatusCode(), 200);

        $headerCollection = $response->getHeaders();
        $this->assertEquals($headerCollection->get('content-type'), 'image/png');
        $this->assertGreaterThan(0, $headerCollection->get('content-length'));
    }
}
