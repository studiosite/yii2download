<?php

namespace studiosite\yii2download\tests;

use yii\di\Container;
use yii\helpers\ArrayHelper;
use Yii;

/**
 * This is the base class for all yii framework unit tests.
 */
abstract class TestCase extends \PHPUnit_Framework_TestCase
{
    /**
     * Clean up after test.
     * By default the application created with [[mockApplication]] will be destroyed.
     */
    protected function tearDown()
    {
        parent::tearDown();
        $this->destroyApplication();
    }

    protected function mockWebApplication($config = [], $appClass = '\yii\web\Application')
    {
        new $appClass(ArrayHelper::merge([
            'id' => 'testapp',
            'basePath' => __DIR__,
            'vendorPath' => dirname(__DIR__) . '/vendor',
            'controllerMap' => [
                'download' => 'studiosite\yii2download\controllers\DownloadController',
            ],
            'components' => [
                'request' => [
                    'cookieValidationKey' => 'wefJDF8sfdsfSDefwqdxj9oq',
                    'scriptFile' => __DIR__ .'/index.php',
                    'scriptUrl' => '/index.php',
                ],
                'image' => [
                    'class' => 'studiosite\yii2download\components\ImageDownload',
                    'allowPath' => [
                        'fixture' => '@studiosite/yii2download/tests/fixture',
                    ],
                    'route' => '/download/image',
                    'filters' => [
                        //'watermark' => '\\common\\componets\Watermark',
                    ],
                    'assetsFolder' => '@studiosite/yii2download/runtime',
                    'baseAssetsFolder' => '@studiosite/yii2download/runtime',
                    'componentNameForController' => 'image',
                    'prohibitPrepublish' => false,
                    'privateCacheTime' => 6000,
                ],
                'imageWithAccess' => [
                    'class' => 'studiosite\yii2download\components\ImageDownload',
                    'allowPath' => [
                        'fixture' => '@studiosite/yii2download/tests/fixture',
                    ],
                    'route' => '/download/image',
                    'filters' => [
                        //'watermark' => '\\common\\componets\Watermark',
                    ],
                    'assetsFolder' => '@studiosite/yii2download/runtime',
                    'baseAssetsFolder' => '@studiosite/yii2download/runtime',
                    'accessManager' => 'studiosite\yii2download\components\AccessManager',
                    'componentNameForController' => 'image',
                    'prohibitPrepublish' => false,
                    'privateCacheTime' => 6000,
                ],
                'file' => [
                    'class' => 'studiosite\yii2download\components\FileDownload',
                    'allowPath' => [
                        'images' => '@studiosite/yii2download/tests/fixture',
                    ],
                    'route' => '/download/file',
                    'componentNameForController' => 'file',
                ],
            ],
        ], $config));
    }

    /**
     * Destroys application in Yii::$app by setting it to null.
     */
    protected function destroyApplication()
    {
        Yii::$app = null;
        Yii::$container = new Container();
    }
}
