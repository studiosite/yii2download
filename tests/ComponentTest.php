<?php

namespace studiosite\yii2download\tests;

use Yii;
use yii\helpers\Url;

class ComponentTest extends TestCase
{
    public function setUp()
    {
        $this->mockWebApplication();
    }

    public function testSimpleLink()
    {
        $path = Yii::$app->image->path('@studiosite/yii2download/tests/fixture/bender.png')->link(true);
        $this->assertFileExists(Yii::getAlias($path));
    }

    public function testThumbnail()
    {
        $path = Yii::$app->image
            ->path('@studiosite/yii2download/tests/fixture/bender.png')
            ->thumbnail([
                'size' => 100
            ])
            ->link(true);
        $this->assertFileExists(Yii::getAlias($path));
    }
}
