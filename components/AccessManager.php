<?php

namespace studiosite\yii2download\components;

use studiosite\yii2download\interfaces\AccessManagerInterface;

/**
 * Демонстрационный доступ мененджер
 *
 * @copyright Студия.сайт
 * @author fromtuba <fromtuba@mail.ru>
 * @author Evgeny Kukharik <jonegkk9@gmail.com>
 */
class AccessManager implements AccessManagerInterface
{
    /**
    * Проверка возможности отдачи файла
    *
    * @param \studiosite\yii2download\models\Object $file
    * @return boolean
    */
    public function checkAccess(\studiosite\yii2download\models\Object $file)
    {
        return true;
    }
}
