<?php

namespace studiosite\yii2download\components;

use studiosite\yii2download\models\FileObject;
use studiosite\yii2download\DownloadInstance;
use Yii;

/**
 * Компонент формирования ссылок на файлы
 *
 * @copyright Студия.сайт
 * @author fromtuba <fromtuba@mail.ru>
 * @author Evgeny Kukharik <jonegkk9@gmail.com>
 */
class FileDownload extends Download
{
    /**
    * @var string Роут отдачи изображения
    */
    public $route = '/download/file';

    /**
    * @var string Имя компонента, для связи с контроллером
    */
    public $componentNameForController = 'file';

    /**
    * Допустимые дефолтные фильтры
    *
    * @return array
    */
    public function defaultFilters()
    {
        return [
            'compress' => '\studiosite\yii2download\filters\CompressFilter',
        ];
    }

    /**
    * Запуск компонента, установка имени файла
    *
    * @param string $fileName имя файла
    * @return studiosite\yii2download\DownloadInstance
    */
    public function path($fileName)
    {
        if (!is_file(Yii::getAlias($fileName)) || !file_exists(Yii::getAlias($fileName))) {
            throw new Exception(Yii::t('studiosite/yii2-download', 'File not found'), 404);
        }

        $model = new FileObject(['fileName' => $fileName, 'publishDirectory' => $this->assetsFolder]);

        return new DownloadInstance($this, $model);
    }
}
