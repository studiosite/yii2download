<?php

namespace studiosite\yii2download\components;

use Yii;
use yii\helpers\ArrayHelper;
use yii\base\Exception;

/**
 * Абстрактный класс компонента формирования ссылки
 *
 * @copyright Студия.сайт
 * @author fromtuba <fromtuba@mail.ru>
 * @author Evgeny Kukharik <jonegkk9@gmail.com>
 */
abstract class Download extends \yii\base\Component
{
    /**
    * @var string Путь куда публиковать файлы для отдачи
    */
    public $assetsFolder = '@app/web/assets';

    /**
    * @var string Базовый путь для отдачи ссылки
    */
    public $baseAssetsFolder = '@web/assets';

    /**
    * @var array Список директорий из которых допустима отдача файлов [имя роута] => роут
    */
    public $allowPath = [];

    /**
    * @var string Имя класса проверки прав отдачи файла
    */
    public $accessManager = '';

    /**
    * @var string Роут отдачи
    */
    public $route = '';

    /**
    * @var string Допустимые фильтры
    */
    public $filters = [];

    /**
    * @var boolean Допустим ли редирект в ссылках, если да, то в незащищенной отдаче будет происходить редирект на временные файлы, что бы разгрузить php
    */
    public $allowRedirect = true;

    /**
    * @var boolean Запретить прегенерацию темп файла до контроллера для незащищенной отдачи
    */
    public $prohibitPrepublish = false;

    /**
    * @var integer Время приватного кеширования
    */
    public $privateCacheTime = 6000; // 100 минут

    /**
    * @var string Имя компонента, для связи с контроллером
    */
    public $componentNameForController = 'download';

    /**
    * @var \studiosite\yii2download\interfaces\AccessManagerInterface Менеджер доступа
    */
    private $accessManagerComponent;

    /**
    * Допустимые дефолтные фильтры
    * @return array
    */
    public function defaultFilters()
    {
        return [];
    }

    /**
    * Получить объект мененджера доступа
    *
    * @return \studiosite\yii2download\interfaces\AccessManagerInterface
    */
    public function getAccessManager()
    {
        if (!empty($this->accessManagerComponent) || empty($this->accessManager))
            return $this->accessManagerComponent;

        $accessManagerClass = $this->accessManager;

        return $this->accessManagerComponent = new $accessManagerClass();
    }

    /**
    * Получить список фильтров
    *
    * @return array
    */
    public function getFilterList()
    {
        return ArrayHelper::merge($this->defaultFilters(), $this->filters);
    }

    /**
    * Получить ссылку на файл в зависимости от конфигурации роутов
    *
    * @return string Путь к файлу
    */
    public function shapeFileName($fileName)
    {
        if (empty($this->allowPath)) {
            throw new Exception(Yii::t(
                'studiosite/yii2-download',
                'The allowed paths for the {componentName} component can\'t be empty',
                ['componentName' => $this->component->componentNameForController]
            ), 403);
        }

        foreach ($this->allowPath as $route => $path) {
            $route.= DIRECTORY_SEPARATOR;
            $path = Yii::getAlias($path).DIRECTORY_SEPARATOR;

            if (strstr($fileName, $route)!==false) {
                $count = 1;
                return str_replace($route, $path, $fileName, $count);
            }
        }

        throw new Exception(Yii::t(
            'studiosite/yii2-download',
            'Access to the path is denied for the {componentName} component',
            ['componentName' => $this->component->componentNameForController]
        ), 403);
    }
}
