<?php

namespace studiosite\yii2download\components;

use studiosite\yii2download\models\ImageObject;
use studiosite\yii2download\DownloadInstance;
use Yii;
use yii\base\Exception;

/**
 * Компонент формирования ссылок на изображения
 *
 * @copyright Студия.сайт
 * @author fromtuba <fromtuba@mail.ru>
 * @author Evgeny Kukharik <jonegkk9@gmail.com>
 */
class ImageDownload extends Download
{
    /**
    * @var string Роут отдачи изображения
    */
    public $route = '/download/image';

    /**
    * @var string Имя компонента, для связи с контроллером
    */
    public $componentNameForController = 'download';

    /**
    * Допустимые дефолтные фильтры
    *
    * @return array
    */
    public function defaultFilters()
    {
        return [
            'thumbnail' => '\studiosite\yii2download\filters\ThumbnailFilter',
            'convert' => '\studiosite\yii2download\filters\ConvertFilter',
            'watermark' => '\studiosite\yii2download\filters\WatermarkFilter',
        ];
    }

    /**
    * Запуск компонента, установка имени файла
    *
    * @param string $fileName имя файла
    * @return studiosite\yii2download\DownloadInstance
    */
    public function path($fileName, $notFoundFile = null)
    {
        Yii::trace('studiosite/yii2-download', 'Set path '.$fileName);

        if (!is_file(Yii::getAlias($fileName)) || !file_exists(Yii::getAlias($fileName))) {
            $fileName = $notFoundFile;

            if (!is_file(Yii::getAlias($fileName)) || !file_exists(Yii::getAlias($fileName)))
                throw new Exception(Yii::t('studiosite/yii2-download', 'File not found'), 404);
        }

        $model = new ImageObject(['fileName' => $fileName, 'publishDirectory' => $this->assetsFolder]);
        Yii::trace('studiosite/yii2-download', 'Create DownloadInstance '.$fileName);
        
        return new DownloadInstance($this, $model);
    }
}
