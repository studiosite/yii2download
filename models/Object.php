<?php

namespace studiosite\yii2download\models;

use Yii;
use yii\base\Exception;

/**
 * Объект файла
 *
 * @copyright Студия.сайт
 * @author fromtuba <fromtuba@mail.ru>
 * @author Evgeny Kukharik <jonegkk9@gmail.com>
 */
class Object extends \yii\base\Object
{
    /**
    * @var mixen object Объект контента
    */
    private $_content;

    /**
    * @var string Путь к изображению
    */
    public $path;

    /**
    * @var string Имя
    */
    public $name;

    /**
    * @var string Расширение
    */
    public $extention;

    /**
    * @var array Примененные фильтры к объекту, потребуется для сохранения
    */
    public $appliedFilters = [];

    /**
    * @var array Хеш опции для слежения уникальности файла
    */
    public $hashOptions = [];

    /**
    * @var string Путь публикации темпов, должно быть установлено перед публикацией
    */
    public $publishDirectory = '';

    /**
    * Получить список опций сохранения
    *
    * @return array
    */
    public function getSaveOption()
    {
        return [];
    }

    /**
    * Получить полный путь к изображению. Исходное изображение
    *
    * @return string
    */
    public function getFileName()
    {
        return realpath(Yii::getAlias($this->path.DIRECTORY_SEPARATOR.$this->name.".".$this->extention));
    }

    /**
    * Получить хешь результативного изображения
    *
    * @return string
    */
    public function getPublishHash()
    {
        return substr(md5($this->getFileName()."-".filemtime($this->getFileName())."-".serialize($this->hashOptions)), 0, 5);
    }

    /**
    * Получить имя файла, берется из имени оригинала
    *
    * @return string
    */
    public function getPublishName()
    {
        return $this->name;
    }

    /**
    * Получить расширение
    *
    * @return string
    */
    public function getPublishExtention()
    {
        return $this->extention;
    }

    /**
    * Получить имя файла для публикации
    *
    * @return string
    */
    public function getPublishBaseName()
    {
        return $this->getPublishName()."-".$this->getPublishHash().".".$this->getPublishExtention();
    }

    /**
    * Получить опубликованный файл
    * Проверяем существования опубликованного файла, если нет, то публикуем
    *
    * @return false|string
    */
    public function getPublishFileName()
    {
        return Yii::getAlias($this->publishDirectory).DIRECTORY_SEPARATOR.$this->getPublishBaseName();
    }

    /**
    * Установка имени файла. При инициализации, что бы разбить имя на имя путь расширение
    *
    * @param $fileName Полное имя файл
    */
    public function setFileName($fileName)
    {
        $fileName = realpath(Yii::getAlias($fileName));

        $this->name = pathinfo($fileName, PATHINFO_FILENAME);
        $this->path = pathinfo($fileName, PATHINFO_DIRNAME);
        $this->extention = pathinfo($fileName, PATHINFO_EXTENSION);
    }

    /**
    * Добавить опции хеширования
    *
    * @param string $key Ключ
    * @param mixen $value Значение
    */
    public function addHashOption($key, $value)
    {
        if (!is_array($value)) {
            $value = [$value];
        }

        $valueConvert = [];

        foreach ($value as $key => $item) {
            $valueConvert[(string) $key] = (string) $item;
        }

        $this->hashOptions[$key] = $valueConvert;
    }

    /**
    * Наложение фильтров
    *
    * @param array $filters Список фильтров
    */
    public function appendFilter($filters)
    {
        if (!empty($filters)) {
            foreach ($filters as $filterClass => $options) {

                $filterName = @$options['_filterName'];
                unset($options['_filterName']);

                $this->addHashOption($filterName, $options);
                $this->appliedFilters[$filterClass] = $options;

                $beforeAppendFilter = new $filterClass();
                $beforeAppendFilter->beforeAppend($this, $options);
            }
        }
    }

    /**
    * Перед сохранением, применяются фильтры
    */
    public function beforePublish()
    {
        if (!empty($this->appliedFilters)) {

            foreach ($this->appliedFilters as $filterClass => $options) {
                $filter = new $filterClass();
                $filter->filter($this, $options);
            }
        }
    }

    /**
    * Удаление опубликованного файла
    *
    * @return boolean
    */
    public function removePublishFile()
    {
        $fileName = Yii::getAlias($this->publishDirectory).DIRECTORY_SEPARATOR.$this->getPublishBaseName();

        if (file_exists($fileName))
            return unlink($fileName);

        return true;
    }

    /**
    * Публикация файла в кеш для отдачи
    *
    * @return boolean|string
    */
    public function publish()
    {
        Yii::trace('studiosite/yii2-download', 'Publish assets file '.$this->getPublishBaseName());

        $this->beforePublish();

        $publishDirectory = Yii::getAlias($this->publishDirectory);
        if (!is_dir($publishDirectory))
            @mkdir($publishDirectory, 0777, true);

        if (!is_dir($publishDirectory))
            throw new Exception(Yii::t(
                'studiosite/yii2-download',
                'The publish directory "{directory}" not exists', [
                'directory'=>$publishDirectory
            ]), 500);

        if (!is_writable($publishDirectory))
            throw new Exception(Yii::t(
                'studiosite/yii2-download',
                'The publish directory "{directory}" not writable', [
                'directory'=>$publishDirectory
            ]), 500);

        $fileName = $publishDirectory.DIRECTORY_SEPARATOR.$this->getPublishBaseName();
        $this->content = $this->content->save($fileName, $this->saveOption);

        if ($this->content)
            return true;

        return false;
    }

    /**
    * Получить контент
    *
    * @return mixen
    */
    public function getContent()
    {
        return $this->_content;
    }

    /**
    * Установить контент
    *
    * @param mixen $content
    */
    public function setContent($content)
    {
        $this->_content = $content;
    }
}
