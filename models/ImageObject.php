<?php

namespace studiosite\yii2download\models;

use Yii;
use yii\imagine\Image;

/**
 * Объект изображения
 *
 * @copyright Студия.сайт
 * @author fromtuba <fromtuba@mail.ru>
 * @author Evgeny Kukharik <jonegkk9@gmail.com>
 */
class ImageObject extends Object
{
    /**
    * @var integer Ширина объекта
    */
    private $width;

    /**
    * @var integer Высота объекта
    */
    private $height;

    /**
    * @var array Опции сохранения, необходимы для указания качества, расширения
    */
    private $saveOption = [
        'quality' => 100, // качество изображения (0..100)
        'extention' => '',  // расширение
    ];

    /**
    * Получить список опций сохранения
    *
    * @return array
    */
    public function getSaveOption()
    {
        return $this->saveOption;
    }

    /**
    * Получить ширину изображения
    *
    * @return integer
    */
    public function getWidth()
    {
        if ($this->width===NULL)
            return $this->width = $this->content->getSize()->getWidth();

        return $this->width;
    }

    /**
    * Получить высоту изображения
    *
    * @return integer
    */
    public function getHeight()
    {
        if ($this->height===NULL)
            return $this->height = $this->content->getSize()->getHeight();

        return $this->height;
    }

    /**
    * Указать качество изображения
    *
    * @param integer $quality Качество
    */
    public function setQuality($quality)
    {
        $this->saveOption['quality'] = $quality;
    }

    /**
    * Указать расширение изображения
    *
    * @param string $extention расширение
    */
    public function setExtention($extention)
    {
        $this->saveOption['extention'] = $extention;
    }

    /**
    * Получить расширение, в зависимости от опций сохранения может измениться
    *
    * @return string
    */
    public function getPublishExtention()
    {
        return empty($this->saveOption['extention']) ? $this->extention : $this->saveOption['extention'];
    }

    /**
    * Получить контент
    *
    * @return mixen
    */
    public function getContent()
    {
        $content = parent::getContent();
        if (!$content) {

            Yii::trace('studiosite/yii2-download', 'Init imagine '.$this->getFileName());
            parent::setContent(Image::getImagine()->open($this->getFileName()));

            return parent::getContent();
        }

        return $content;
    }
}
