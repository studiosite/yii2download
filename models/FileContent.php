<?php

namespace studiosite\yii2download\models;

use Yii;

/**
 * Объект контента файла
 *
 * @copyright Студия.сайт
 * @author fromtuba <fromtuba@mail.ru>
 * @author Evgeny Kukharik <jonegkk9@gmail.com>
 */
class FileContent extends \yii\base\Object
{
    /**
    * @var string Исходное имя файла
    */
    public $sourceFileName = '';

    /**
    * Получить объект модели
    *
    * @return self
    */
    public static function create($fileName)
    {
        return new self(['sourceFileName' => $fileName]);
    }

    /**
    * Сохранения контанета с опциями
    *
    * @param string $fileName Имя файла для публикации
    * @param array $options Опции сохранения
    */
    public function save($fileName, $options = [])
    {
        if ($options['callback']) {
            $callback = $options['callback'];

            if (!$callback($this, $fileName, $options))
                return false;

            return $this;
        }

        if (!copy($this->sourceFileName, $fileName))
            return false;

        return $this;
    }
}
