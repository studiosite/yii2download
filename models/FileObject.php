<?php

namespace studiosite\yii2download\models;

use Yii;
use studiosite\yii2download\models\FileContent;

/**
 * Объект файла
 *
 * @copyright Студия.сайт
 * @author fromtuba <fromtuba@mail.ru>
 * @author Evgeny Kukharik <jonegkk9@gmail.com>
 */
class FileObject extends Object
{
    /**
    * @var array Опции сохранения, необходимы для указания качества, расширения
    */
    private $saveOption = [
        'extention' => '', // расширение файла
        'callback' => false, // функция для выполнения перед отдачей
    ];

    /**
    * Получить список опций сохранения
    *
    * @return array
    */
    public function getSaveOption()
    {
        return $this->saveOption;
    }

    /**
    * Указать расширение файла, для вильтра конвертеров
    *
    * @param string $extention расширение
    */
    public function setExtention($extention)
    {
        $this->saveOption['extention'] = $extention;
    }

    /**
    * Указать расширение файла, для вильтра конвертеров
    *
    * @param string $extention расширение
    */
    public function setCallback($callback)
    {
        $this->saveOption['callback'] = $callback;
    }

    /**
    * Получить расширение, в зависимости от опций сохранения может измениться
    *
    * @return string
    */
    public function getPublishExtention()
    {
        return empty($this->saveOption['extention']) ? $this->extention : $this->saveOption['extention'];
    }

    /**
    * Получить контент
    *
    * @return mixen
    */
    public function getContent()
    {
        $content = parent::getContent();
        if (!$content) {

            Yii::trace('studiosite/yii2-download', 'Init FileContent '.$this->getFileName());
            parent::setContent(FileContent::create($this->getFileName()));

            return parent::getContent();
        }

        return $content;
    }
}
