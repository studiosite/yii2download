<?php
/**
 * Message translations.
 */
return [
    'File "{name}.{extention}" not found' => 'Файл "{name}.{extention}" не найден',
    'Access is denied' => 'Доступ запрещен',
    'Failed to create a folder for the preview images' => 'Не удалось создать директорию для превью изображений',
    'Failed to create a preview of the file "{name}.{extention}"' => 'Не удалось создать превью изображения',
    'File not found' => 'Файл не найден',
    'Can not publish image file' => 'Не удалось опубликовать изображение',
    'Invalid query' => 'Не верный запрос',
    'Image not found' => 'Изображение не найдено',
    'Filter {filterName} not found' => 'Фильтр {filterName} не найден',
    'The allowed paths for the {componentName} component can\'t be empty' => 'Список допустимых директорий для компонента {componentName}"не может быть пустым',
    'Access to the path is denied for the {componentName} component' => 'Доступ по указанному пути для компонента {componentName} запрещен',
    'Component name or the file name is not specified' => 'Не указано имя компонента или файла',
    'The publish directory "{directory}" not exists' => 'Директория для публикации "{directory}" не существует',
    'The publish directory "{directory}" not writable' => 'Директория для публикации "{directory}" не доступна для записи',
    'Watermark image not found' => 'Изображение водяного знака не найдено',
    'Invalid watermark size' => 'Не верный задан размер водяного знака',

];
