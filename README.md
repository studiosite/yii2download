yii2download
=================

Компонент формирования ссылок для скачивания (отдачи) изображений и файлов с наложением фильтров

## Установка

Предпочтительный способ установить это расширение через композитор. [composer](http://getcomposer.org/download/).

Запустите

```
$ php composer.phar require studiosite/yii2download "*"
```

или добавить

```
"studiosite/yii2download": "*"
```

в секции ```require``` `composer.json` файла.

## Использование

### Yii2 Download

В конфигурации приложения необходимо указать карту контроллера на `studiosite\yii2download\controllers\DownloadController`, либо на унаследованный

```php
...
...
'controllerMap' => [
    'download' => 'studiosite\yii2download\controllers\DownloadController',
    ...
],
...
...
```

и сконфигурировать компонент(-ы) формирования ссылок на изображение (`studiosite\yii2download\components\ImageDownload`) и на файл (`studiosite\yii2download\components\FileDownload`)

```php
...
'components' => [
    ...
    'image' => [
        // Класс компонента (Обязательно для конфигурирования)
        'class' => 'studiosite\yii2download\components\ImageDownload',

        // Список директорий из которых допустима отдача файлов [имя роута] => роут (Обязательно для конфигурирования)
        // В динамических ссылка будет заменяться абсолютной ссылки на файл на роут (На пример путь к файлу /var/www/project/common/data/image/user/avatar.png будет изменен на user/avatar.png)
        'allowPath' => [
            'user' => '@common/data/images/user',
            'images' => '@common/data/images',
        ],

        // Роут отдачи, сконфигурированный выше в controllerMap. Для изображений используется экшен image (Обязательно для конфигурирования) (По умолчанию '/download/image')
        'route' => '/download/image',

        // Конфигурирование пользовательских фильтров. Фильтр по умолчанию (studiosite\yii2download\components\ImageDownload::defaultFilters()) будут склеины с пользовательскими (По умолчанию [])
        'filters' => [
            //'watermark' => '\\common\\componets\Watermark',
        ],

        // Путь куда публиковать файлы для отдачи (По умолчанию '@app/web/assets')
        'assetsFolder' => '@app/web/assets/images',

        // Базовый путь для отдачи ссылки (По умолчанию '@web/assets')
        'baseAssetsFolder' => '@web/assets/images',

        // Имя класса проверки прав отдачи файла.
        // Используется при необходимости защищенной отдачи конкретным пользователям
        // Класс проверки долже соответствовать интерфейсу studiosite\yii2download\interfaces\AccessManagerInterface
        // (По умолчанию не указан, проверки не будет)
        'accessManager' => 'studiosite\yii2download\components\AccessManager',

        // Имя этого компонента для дальнейшей обработки в контроллере. Yii::$app->image
        // (По умолчанию 'image')
        'componentNameForController' => 'image',

        // Запретить прегенерацию темп файла до контроллера для незащищенной отдачи (По умолчанию false).
        // При указании true может уменьшить нагрузку при отрисовки страниц с большим списком изображений.
        // Ссылка получится динамической, может потребоваться для передачи через апи, что бы запрашивавший сервер не потерял ссылку при очестве assets
        'prohibitPrepublish' => false,

        // Время приватного кеширования в секундах для динамических ссылок (По умолчанию 100 минут (6000))
        'privateCacheTime' => 6000,
    ],
    'file' => [

        // Класс компонента (Обязательно для конфигурирования)
        'class' => 'studiosite\yii2download\components\FileDownload',

        // Список директорий из которых допустима отдача файлов [имя роута] => роут (Обязательно для конфигурирования)
        'allowPath' => [
            'images' => '@common/data/images/content',
        ],

        // Роут отдачи, сконфигурированный выше в controllerMap. Для изображений используется экшен image (Обязательно для конфигурирования) (По умолчанию '/download/file')
        'route' => '/download/file',

        // Имя этого компонента для дальнейшей обработки в контроллере. Yii::$app->file
        // (По умолчанию 'file')
        'componentNameForController' => 'file',

        // Все остальные параметры по аналогии с компонентом image
        ...
    ]
    ...
...
```

Компоненты используются по шаблону `Yii::$app->ИМЯ_КОМПОНЕНТА->path(АБСОЛЮТНЫЙ_ПУТЬ_ДО_ИЗОБРАЖЕНИЯ)->ИМЯ_ФИЛЬТРА(ПАРАМЕТРЫ_ФИЛЬТРА)->link();`

Для компонента изображений, функция `path` принимает 2 параметра:
- fileName - абсолютный путь к оригиналу изображения
- notFoundFile - абсолютный путь к изображению в случае если не найден оригинальный файл

Пример использования:

```php
...

// Изменит размер исходного изображение до 1000 на 1000 px, качество 100 качество 90 % и конвертирует в формат image/jpeg
echo
// результат с указаной выше конфигурацией будет (Доступ менеджер указан, ссылка динамическая) http://example.com/download/image?name=user%2Fno-image.png&component=image&thumbnail%5Bsize%5D=1000&thumbnail%5Bquality%5D=90&convert%5Bextention%5D=jpg
 Yii::$app->image->path('@common/data/images/user/no-image.png')->thumbnail([
    'size' => 1000,
    'quality' => 90,
])->convert([
    'extention' => 'jpg'
])->link();

// Конвертирует в формат image/jpeg
// результат с указаной выше конфигурацией будет (Доступ менеджер указан, ссылка динамическая) http://example.com/download/image?name=user%2Fno-image.png&component=image&convert%5Bextention%5D=JPEG
echo  Yii::$app->image->path('@common/data/images/user/no-image.png')->convert([
    'extention' => 'JPEG'
])->link();

// Сожмет файл (используется компонент https://github.com/wapmorgan/UnifiedArchive). На момент написания доступен долько дефолтный алгорим сжатия (формат zip)
// результат с указаной выше конфигурацией будет (Доступ менеджер не указан) http://example.com/assets/images/no-image-14bf6.zip Ссылка из асетсов
echo  Yii::$app->file->path('@common/data/images/user/no-image.png')->compress()->link();

// Просто отдача файла
// результат с указаной выше конфигурацией будет (Доступ менеджер не указан) http:///example.com/assets/images/no-image-8808b.png
echo  Yii::$app->file->path('@common/data/images/user/no-image.png')->link();

// Отдача файла с возможным несуществованием оригинала
// результат с указаной выше конфигурацией будет (Доступ менеджер не указан) http:///example.com/assets/images/no-image-8808b.png
echo  Yii::$app->file->path('@common/data/images/user/file.png', '@common/data/images/user/no-image.png')->link();
...
```

В компоненте ImageDownload

## Фильтры

В комплект пакета входит 3 фильтра изображений
- [convert](https://bitbucket.org/studiosite/yii2download/raw/master/filters/ConvertFilter.php)
- [thumbnail](https://bitbucket.org/studiosite/yii2download/raw/master/filters/ThumbnailFilter.php)
- [watermark](https://bitbucket.org/studiosite/yii2download/raw/master/filters/WatermarkFilter.php)

и 1 фильтр файлов
- [compress](https://bitbucket.org/studiosite/yii2download/raw/master/filters/CompressFilter.php)

### Фильтры изображений

#### convert - Конвертирование изображения в другой формат

Применяется с 1 параметром `extention`, по умолчанию - `null`.
Модель изображения является `yii\imagine\Image` объект, п/у возможные форматы зависят от используемого движка

> Фильтр может использоваться с другими фильтрами изображений

#### thumbnail - Фильтр сжатия изображения по размеру и качеству

Принимает параметры:
- `size` - размер стороны квадрата `size=width=height`. Опция приоритетнее `width` и `height`
- `width` - ширина изображения. По умолчанию остается прежней
- `height` - высота изображения. По умолчанию остается прежней
- `quality` - качество изображение (100 - изображение не сжимается). По умолчанию 100%

Все параметры не обязательны

> Фильтр может использоваться с другими фильтрами изображений

#### watermark - Наложения водяного знака на изображение

Принимает параметры:
- `image` - абсолютный путь до изображения водяного знака
- `right` - положение от правого края (не обязательный параметр)
- `left` - положение от левого края (не обязательный параметр)
- `top` - положение от верха (не обязательный параметр)
- `bottom` - положение от низа (не обязательный параметр)
- `resize` - изменить размер знака (1 не менять, 1.1 увелечить на 10%) (По умолчанию 1)

> Фильтр может использоваться с другими фильтрами изображений

### Фильтры файлов

#### compress - Сжатия файла

Принимает 1 параметр `format`, по умолчанию `zip`. Подробнее о поддерживаемых форматах [wapmorgan/UnifiedArchive](https://github.com/wapmorgan/UnifiedArchive)

> Фильтр НЕ может использоваться с другими фильтрами изображений, окончания фильтрации является callback сжатия


Тестирование
-------
Перейти в папку с пакетом и установить зависимости
```shell
cd studiosite/yii2download
composer install
```
Далее для тестирования в том же месте

```
phpunit
```

