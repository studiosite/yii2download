<?php

namespace studiosite\yii2download\controllers;

use Yii;
use yii\web\Controller;
use yii\base\Exception;
use yii\web\NotFoundHttpException;
use studiosite\yii2download\helpers\DownloadHelper;

/**
 * Контроллер отдачи файлов. Необходимо наследоваться или указывать в controller map
 *
 * @copyright Студия.сайт
 * @author fromtuba <fromtuba@mail.ru>
 * @author Evgeny Kukharik <jonegkk9@gmail.com>
 */
class DownloadController extends Controller
{
    /**
    * @var array Фильтры приминяемые к файлу
    */
    protected $filters = [];

    /**
    * @var string Имя файла для компонента, который сформировал ссылку
    */
    protected $fileName;

    /**
    * @var \studiosite\yii2download\components\Download Компонент сформировавший ссылку
    */
    protected $component;

    /**
    * Экшен отдачи изображений
    *
    * @param string $_GET['name'] имя файла и путь
    * @param string $_GET['component'] имя компонента
    * @param array $_GET[MIXEN] массив фильтр[имя параметра] => настройки
    * @return file
    */
    public function actionImage()
    {
        return $this->responce('\studiosite\yii2download\models\ImageObject');
    }

    /**
    * Экшен отдачи файлов
    *
    * @param string $_GET['name'] имя файла и путь
    * @param string $_GET['component'] имя компонента
    * @param array $_GET[MIXEN] массив фильтр[имя параметра] => настройки
    * @return file
    */
    public function actionFile()
    {
        return $this->responce('studiosite\yii2download\models\FileObject');
    }

    /**
    * Отдача файла по классу объекта
    *
    * @param string $modelType Имя класса модели
    * @return file
    */
    public function responce($modelType)
    {
        if (!file_exists($this->fileName))
            throw new NotFoundHttpException(Yii::t('studiosite/yii2-download', 'File not found'));
            

        $model = new $modelType(['fileName' => $this->fileName, 'publishDirectory' => $this->component->assetsFolder]);
        $model->appendFilter($this->filters);

        $accessManager = $this->component->getAccessManager();

        if (!empty($accessManager) && !$this->component->getAccessManager()->checkAccess($model)) {
            throw new Exception(Yii::t('studiosite/yii2-download', 'Access is denied'), 403);
        }

        // Если не безопасная отдача и допустим редирект на асетс файл
        if (empty($accessManager) && $this->component->allowRedirect) {
            $fileName = $model->getPublishFileName();

            if (!file_exists($fileName))
                $model->publish();

            return $this->redirect($this->component->baseAssetsFolder.DIRECTORY_SEPARATOR.$model->getPublishBaseName());
        }

        $fileName = $model->getFileName();

        // Если были применены фильтры, то публикуем в асетсы
        // После отдачи, удаляем файл
        if (!empty($this->filters)) {
            $fileName = $model->getPublishFileName();

            if (!file_exists($fileName))
                $model->publish();

            $this->on(self::EVENT_AFTER_ACTION, function () use ($fileName) {
                unlink($fileName);
            });
        }

        \yii::$app->response->headers->set('Cache-Control', 'private, max-age='.intval($this->component->privateCacheTime));
        \yii::$app->response->headers->set('Pragma', 'private');

        return \yii::$app->response->sendFile($fileName);
    }

    /**
    * Перед экшеном резолвятся гет параметы
    * Подключается компонент
    * Считывается имя файла
    * Записываются фильтра
    *
    * @param \yii\base\Action $action
    */
    public function beforeAction($action)
    {
        if (!parent::beforeAction($action)) {
            return false;
        }

        $componentName = Yii::$app->request->get('component', false);
        $fileName = Yii::$app->request->get('name', false);

        if (!$componentName || !$fileName) {
            throw new Exception(Yii::t('studiosite/yii2-download', 'Invalid query').". ".Yii::t('studiosite/yii2-download', 'Component name or the file name is not specified'), 400);
        }

        $this->component = Yii::$app->$componentName;
        $this->fileName = $this->component->shapeFileName($fileName);
        $this->filters = DownloadHelper::resolveFilters(Yii::$app->request->get(), $this->component->getFilterList());

        return true;
    }
}
