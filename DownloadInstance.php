<?php

namespace studiosite\yii2download;

use studiosite\yii2download\components\Download as DownloadComponent;
use studiosite\yii2download\models\Object;
use Yii;
use yii\base\Exception;
use yii\helpers\ArrayHelper;

/**
 * Экземпляр изолирует в себе работу с отдельно взятой моделью
 * при использовании компонента
 *
 * TODO что будет если после вызова link продолжить наложение фильтров и снова вызвать link?
 */
class DownloadInstance
{
    /**
     * @var studiosite\yii2download\components\Download Компонент, который инициировал загрузку
     */
    protected $component;

    /**
     * @var studiosite\yii2download\models\Object Модель файла
     */
    protected $model;

    /**
     * @var array Список парметров запроса на файл
     */
    private $params = [];

    /**
     * @param studiosite\yii2download\components\Download $component
     * @param studiosite\yii2download\models\Object $object
     */
    public function __construct(
        DownloadComponent $component,
        Object $model
    ) {
        $this->component = $component;
        $this->model = $model;

        if (!$this->model->getFileName()) {
            throw new Exception(Yii::t('studiosite/yii2-download', 'Image not found'), 404);
        }

        $this->setParam('name', $this->getLink($this->model->getFileName()));
        $this->setParam('component', $this->component->componentNameForController);
    }

    /**
     * Перехват методов, для определения фильтрации
     *
     * @return studiosite\yii2download\DownloadInstance
     */
    public function  __call($method, $params)
    {
        $filters = $this->component->getFilterList();

        if (empty($filters) || empty($filters[$method])) {
            throw new Exception(Yii::t(
                'studiosite/yii2-download',
                'Filter {filterName} not found',
                ['filterName' => $method]
            ), 400);
        }

        $filter = $filters[$method];

        // параметры, полученные при выозове фильтра, необходимо сохранить
        // для последующей генерации ссылки на контроллер
        $clearParams = current($params) ?: [];
        $this->setParam($method, $clearParams);
        $this->model->addHashOption($method, $clearParams);
        $this->model->appendFilter([$filter => $clearParams]);

        // Помимо применяем к моделе beforeAppend фильтра для сохранения соединения с контроллером
        // TODO что-то не так с комментарием выше
        $filterModel = new $filter();
        $filterModel->beforeAppend($this->model, $params);

        return $this;
    }

    /**
     * Сформировать ссылку на файл
     * Если возможна небезопасная отдача, то проверяем опубликован ли файл в асетсах
     * и отдаем прямую ссылку минуя контроллер отдачи
     *
     * @param boolean $prepublish Препубликация перед отдачей ссылки, Работает только в случае если возможна небезопасная отдача
     * @return array|string route
     */
    public function link($prepublish = false)
    {
        $accessManager = $this->component->getAccessManager();

        // Если отдача не защищенная
        if (empty($accessManager) && (!$this->component->prohibitPrepublish || $prepublish)) {
            $fileName = $this->model->getPublishFileName();
            $published = false;
            
            if (!file_exists($fileName) && $prepublish && $this->model->publish()) {
                $published = true;
            } else if (file_exists($fileName)) {
                $published = true;
            }

            if ($published) {
                Yii::trace('studiosite/yii2-download', 'Assets file exists - '.$this->model->getPublishBaseName());

                $link = $this->component->baseAssetsFolder;
                $link .= DIRECTORY_SEPARATOR . $this->model->getPublishBaseName();

                return $link;
            }
        }

        $link = ArrayHelper::merge([$this->component->route], $this->params);

        return $link;
    }

    /**
     * Добовить параметер в запрос
     *
     * @param string|integer $key Ключ
     * @param mixen|array $value Значение
     *
     * @return studiosite\yii2download\DownloadInstance
     */
    protected function setParam($key, $value)
    {
        $this->params[$key] = $value;

        return $this;
    }

    /**
    * Получить ссылку к файлу используя список разрешенных директорий компонента
    *
    * @return string Путь к файлу
    */
    protected function getLink($fileName)
    {
        $fileName = Yii::getAlias($fileName);
        if (empty($this->component->allowPath))
            throw new Exception(Yii::t(
                'studiosite/yii2-download',
                'The allowed paths for the {componentName} component can\'t be empty',
                ['componentName' => $this->component->componentNameForController]
            ), 403);

        foreach ($this->component->allowPath as $route => $path) {
            $route .= DIRECTORY_SEPARATOR;
            $path = Yii::getAlias($path) . DIRECTORY_SEPARATOR;

            if (strstr($fileName, $path) !== false) {
                $count = 1;
                return str_replace($path, $route, $fileName, $count);
            }
        }

        throw new Exception(Yii::t(
            'studiosite/yii2-download',
            'Access to the path is denied for the {componentName} component',
            ['componentName' => $this->component->componentNameForController]
        ), 403);
    }
}
