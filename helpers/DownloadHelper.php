<?php

namespace studiosite\yii2download\helpers;

/**
 * Хелпер отдачи файлов (изображений)
 *
 * @copyright Студия.сайт
 * @author fromtuba <fromtuba@mail.ru>
 * @author Evgeny Kukharik <jonegkk9@gmail.com>
 */
class DownloadHelper
{
    /**
    * Получить список фильтров из запроса
    *
    * @param array $params гет запрос с параметрами
    * @param array $allowFilters конфигурация допустимых фильтров
    * @return array класс фильтра => опции фильтрации
    */
    public static function resolveFilters(array $params, array $allowFilters)
    {
        $result = [];

        if (!empty($params)) {
            foreach ($params as $filterName => $filterOptions) {

                if (empty($allowFilters[$filterName]))
                    continue;

                $options = [];

                if (is_array($filterOptions)) {
                    $options = $filterOptions;
                }

                $options['_filterName'] = $filterName; // Сохраняем искомое имя фильтра
                $result[$allowFilters[$filterName]] = $options;
            }
        }

        return $result;
    }
}
