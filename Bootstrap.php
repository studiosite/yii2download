<?php

/**
 * Компонент формирования ссылок на изображения
 *
 * @copyright Студия.сайт
 * @author Evgeny Kukharik <jonegkk9@gmail.com>
 */
namespace studiosite\yii2download;

use Yii;
use yii\base\BootstrapInterface;
use yii\i18n\PhpMessageSource;

class Bootstrap implements BootstrapInterface
{
    /** 
    * @inheritdoc 
    */
    public function bootstrap($app)
    {
        Yii::setAlias('@studiosite/yii2download', __DIR__);

        if (!isset($app->i18n->translations['studiosite/yii2-download'])) {
            $app->i18n->translations['studiosite/yii2-download'] = [
                'class' => 'yii\i18n\PhpMessageSource',
                'sourceLanguage' => 'en-US',
                'basePath' => __DIR__.'/messages',
                'forceTranslation' => true,
                'fileMap' => [
                    'studiosite/yii2-download' => 'messages.php'
                ]
            ];
        }
    }
}
