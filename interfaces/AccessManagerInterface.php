<?php

namespace studiosite\yii2download\interfaces;

/**
 * Интерфейс класса проверки возможности отдачи пользователю файла
 *
 * @copyright Студия.сайт
 * @author fromtuba <fromtuba@mail.ru>
 * @author Evgeny Kukharik <jonegkk9@gmail.com>
 */
interface AccessManagerInterface
{
    /**
    * Проверка возможности отдачи файла
    *
    * @param \studiosite\yii2download\models\Object $file
    * @return boolean
    */
    public function checkAccess(\studiosite\yii2download\models\Object $file);
}
