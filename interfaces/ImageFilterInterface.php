<?php

namespace studiosite\yii2download\interfaces;

/**
 * Интерфейс фильтра изображений
 *
 * @copyright Студия.сайт
 * @author fromtuba <fromtuba@mail.ru>
 * @author Evgeny Kukharik <jonegkk9@gmail.com>
 */
interface ImageFilterInterface
{
    /**
    * Наложение фильтра
    *
    * @param \studiosite\yii2download\models\ImageObject $image
    * @param array $options Параметры фильтра
    */
    public function filter(\studiosite\yii2download\models\ImageObject &$image, array $options);

    /**
    * Перед применением фильтров к моделе
    *
    * @param \studiosite\yii2download\models\ImageObject $image
    * @param array $options Параметры фильтра
    */
    public function beforeAppend(\studiosite\yii2download\models\ImageObject &$image, array $options);
}
